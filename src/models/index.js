const Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');
require('dotenv').config();

let database = process.env.DATABASE,
    username  = process.env.DB_USER,
    password  = process.env.DB_PASS;
    host = process.env.HOST;
//connection
const db = new Sequelize(database,username,password,{
    host:host,
    dialect:'mysql'
});

db.authenticate()
  .then(()=>console.log('Connected to db'))
  .catch((error)=> console.log('Error connecting to db '+error));


const Personnel = require('./Personnel')(db,Sequelize);
const Task = require('./Task')(db,Sequelize);


Personnel.prototype.confirmPassword = (password,hashedPass) =>{
  return bcrypt.compareSync(password,hashedPass);
}
Personnel.beforeCreate((personnel,options)=>{
  return bcrypt.hash(personnel.personnel_password)
               .then(hashPassword =>{
                   personnel.personnel_password = hashPassword;
               });
});

db.sync();

module.exports ={
    Personnel,
    Task
};