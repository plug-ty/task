
module.exports = (db,Sequelize) =>{
   const personnel = db.define('Personnel',{
       personnel_id:{
           type: Sequelize.INTEGER,
           autoIncrement:true,
           primaryKey:true
       },
       personnel_onames:Sequelize.STRING(45),
       personnel_fname:{
           type:Sequelize.STRING(20),
           allowNull:false
       },
       Personnel_email:{
           type:Sequelize.STRING(45),
           validate:{
               isEmail:true
           }
       },
       personnel_phone:Sequelize.STRING(45),
       Personnel_password:{
           type:Sequelize.STRING(100),
           defaultValue:'123456'
       },
       personnel_status:{
           type:Sequelize.BOOLEAN,
           defaultValue:true
       },
       last_login:{
           type:Sequelize.DATE
       },
       personnel_type_id:{
           type:Sequelize.INTEGER(11),
           defaultValue:1
       },
       reset_password:{
           type:Sequelize.BOOLEAN,
           allowNull:false,
           defaultValue:true
       }
   },
   {
    freezeTableName: true,
    timestamps:false
   });
   
   return personnel;
}