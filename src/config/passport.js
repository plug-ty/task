const passport = require('passport'),
      LocalPassport = require('passport-local').Strategy,
      passportJWT = require('passport-jwt'),
      jwtStrategy = passportJWT.Strategy,
      extractJwt  = passportJWT.ExtractJwt;
const Personnel = require('../models').Personnel;
require('dotenv').config();

passport.use(new LocalPassport({
    usernameField:'phone'
},(phone,password,done)=>{
    Personnel.findOne({where:{personnel_phone:phone}})
             .then(user=>{
                 if(!user){ return done(null,false,{message:'Wrong Phone Number!'})}
                if(!user.confirmPassword(password,user.Personnel_password)){
                   return done(null,false,{message:'Wrong Password!'});
                }
                 return done(null,user);
             })
             .catch(error=>{
                 console.log(error);
             })
}));

let secret = process.env.SECRET_KEY,
    issuer = process.env.App_Name;

passport.use(new jwtStrategy({
    jwtFromRequest:extractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey   : secret,
    issuer
},(user,done)=>{
   if(user){
       done(null,user);
   }
}));

module.exports = passport;