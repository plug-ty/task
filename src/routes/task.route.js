const express = require('express'),
      router = express.Router(),
      Task = require('../models').Task,
      Passport = require('../config/passport');

router.get('/assigned',Passport.authenticate('jwt',{session:false}),(req,res)=>{
    
  var {page,limit,order,orderMethod} =  req.query;
  page = parseInt(page) || 1;
  var perPage = parseInt(limit) || 5;
  var offset =(perPage*page)-perPage;

  Task.findAndCountAll()
      .then(result=>{
         var pages = Math.ceil(result.count / perPage);

       Task.findAll({limit:perPage,offset,order:[[order,orderMethod]]})
           .then(tasks=>{
               res.status(200).json({
                   totalTaks:result.count,
                   page,
                   pages,
                   perPage,
                   tasks
               })
           })
      })
});



module.exports = router;