const personnelRouter = require('./personnel.route'),
      TaskRouter = require('./task.route');


module.exports = {
    personnelRouter,
    TaskRouter
}