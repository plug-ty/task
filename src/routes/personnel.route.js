const express = require('express'),
      router = express.Router(),
      Personnel = require('../models').Personnel,
      Passport = require('../config/passport'),
      jwt = require('jsonwebtoken');
      require('dotenv').config();

      let secret = process.env.SECRET_KEY,
      issuer = process.env.App_Name;

router.get('/',(req,res)=>{
    Personnel.findAll()
             .then(personnels=>{
                 res.status(200).json({personnels});
             })
             .catch(error =>{
                 res.status(500).json({error});
             })
});

router.post('/login',(req,res,next)=>{
    Passport.authenticate('local',(error,user,info)=>{
      if(error){
          next(error);
      }
      if(!user){
          res.status(500).json({error:info});
      }

      const accessToken = jwt.sign({user},secret,{expiresIn:'24h',issuer:issuer});
      const myresponse = {
          reset_password:user.reset_password,
          accessToken,
          expires_in:'24h'
      }
      res.json(myresponse);
    })(req,res,next)
})

module.exports = router;