const express = require('express'),
      bodyParser = require('body-parser'),
      app   = express(),
      Passport = require('./src/config/passport');
      require('dotenv').config();

let port = process.env.PORT;

//passport
Passport.serializeUser((user,done)=>{
    done(null,user);
})
app.use(Passport.initialize());

app.use(require('cors')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


//routes
const {personnelRouter, TaskRouter} = require('./src/routes');

app.use('/personnel',personnelRouter);
app.use('/tasks',TaskRouter);
    

app.listen(port,()=>console.log(`localhost:${port}`));